#pragma once
#include <map>

struct SGONode
{
	int type;
	std::wstring value;
};

class SGO
{
public:
	void Read( std::string path );
	void FromData( std::vector< unsigned char > in );

	void Parse();

	void Write();
	char * GenerateHeader();

private:
	std::vector< unsigned char > m_vecRawData;
	std::map< std::wstring, SGONode > m_nodes;
};