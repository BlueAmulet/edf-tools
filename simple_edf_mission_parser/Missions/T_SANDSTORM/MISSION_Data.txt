g_sergent = 18446744073709551615;
g_sergent_follower1 = 18446744073709551615;
g_sergent_follower2 = 18446744073709551615;
g_sergent_follower3 = 18446744073709551615;
leader1;
leader2;
leader3;

Voice2( arg0, arg1 )
{
	relIndex -= 3
	game->Function_4000( RAS[ relIndex ], arg1, arg0 )
	game->Function_0xA2(  )
	game->Function_0xc8( RAS[ relIndex ] )
	relIndex += 1
}
RadioBegin(  )
{
	relIndex -= 1
	game->Function_4000( "MusenBegin" )
	relIndex += 1
}
RadioEnd(  )
{
	relIndex -= 1
	game->Function_4000( "MusenEnd" )
	relIndex += 3
}
RadioVoice( arg0, arg1 )
{
	relIndex -= 3
	game->Function_0xA2(  )
	game->Function_0xc8( RAS[ relIndex ] )
	game->Function_4000( RAS[ relIndex ], arg1, arg0 )
	game->Function_0xA2(  )
	relIndex += 2
}
EconomyMode( arg0 )
{
	relIndex -= 2
	game->Function_0x11( RAS[ relIndex ] )
	relIndex += 2
}
WaitAiMoveEnd( arg0 )
{
	relIndex -= 2
	if( game->NotOnPath( RAS[ relIndex ] ) )
	{
	0x2D
	relIndex += 1
	}
}
InitializeCommon(  )
{
	relIndex -= 1
	game->Function_0x05(  )
	relIndex += 4
}
MissionClear_Common( arg0 )
{
	relIndex -= 4
	0x2D
	if( arg0 )
	{
	game->Function_0x13(  )
	}
	RAS[ relIndex + 2 ] = game->UIElement?( "app:/ui/lyt_HUiMissionCleared.sgo" )
	game->Function_0xc8( RAS[ relIndex ] )
	RAS[ relIndex + 3 ] = game->Function_1E( "ui_fade_screen_simple" )
	game->Function_50( 3, 3, RAS[ relIndex ] )
	game->Function_51( RAS[ relIndex ] )
	game->Function_0x1F( RAS[ relIndex ] )
	game->Function_0x1F( RAS[ relIndex ] )
	game->Function_0x03( true )
	relIndex += 1
}
MissionClear(  )
{
	relIndex -= 1
	game->Function_301( 2 )
	game->Function_0xc8( 1.5 )
	game->Function_0x34( false )
	game->PlayBgm( "Jingle_MissionCleared" )
	relIndex += 1
}
FinalMissionClear(  )
{
	relIndex -= 1
	game->Function_301( 2 )
	game->Function_0xc8( 1.5 )
	game->Function_0x34( false )
	game->PlayBgm( "Jingle_MissionClearedFinal" )
	MissionClear_Common( 10 )
	relIndex += 1
}
MissionEscapeClear(  )
{
	relIndex -= 1
	game->Function_301( 2 )
	game->Function_0xc8( 1.5 )
	game->Function_0x34( false )
	game->PlayBgm( "Jingle_MissionEscape" )
	MissionClear_Common( 7 )
	relIndex += 6
}
MissionGameOverEvent(  )
{
	relIndex -= 6
	0x2D
	game->Function_10001( false, 2, false )
	game->Function_0xc8( 3 )
	game->Function_0x34( false )
	game->Function_301( 2 )
	game->Function_0xc8( 1.5 )
	0x2D
	if( STACK_SIZE_ERROR )
	{
	game->Function_0x13(  )
	}
	RAS[ relIndex + 2 ] = game->UIElement?( "app:/ui/lyt_HUiMissionFailed.sgo" )
	game->PlayBgm( "Jingle_MissionFailed" )
	game->Function_0xc8( 5 )
	game->Function_10002(  )
	0x2D
	if( STACK_SIZE_ERROR )
	{
	game->Function_0x13(  )
	}
	game->Function_0x21( RAS[ relIndex ] )
	game->Function_38( false, " ", " ", relIndex + 3, game->UIElement?( "app:/ui/lyt_HUiFailedResult.sgo" ) )
	RAS[ var ] = var
	RAS[ relIndex + 4 ] = game->Function_1E( "ui_fade_screen_simple" )
	game->Function_50( 0.5, 3, RAS[ relIndex ] )
	game->Function_51( RAS[ relIndex ] )
	game->Function_0x1F( RAS[ relIndex ] )
	game->Function_0x1F( RAS[ relIndex ] )
	game->Function_0x03( 3 )
	game->Function_0x03( 2 )
	relIndex += 5
}
SceneEffect_Snow( arg0, arg1, arg2, arg3 )
{
	relIndex -= 5
	game->SetSnow?( RAS[ relIndex ], 40, RAS[ relIndex ], 100, 2, 2, 2, 0.5, 0.5, 0.5, RAS[ relIndex ], RAS[ relIndex ], 0.05, 0.1, 10 )
	relIndex += 7
}
SceneEffect_Rain( arg0, arg1, arg2, arg3, arg4, arg5 )
{
	relIndex -= 7
	game->SetRain?( RAS[ relIndex ], RAS[ relIndex ], 40, RAS[ relIndex ], 200, 2, 2, 2, 1, RAS[ relIndex ], 0.5, RAS[ relIndex ], RAS[ relIndex ] )
	relIndex += 8
}
SceneEffect_RainEx( arg0, arg1, arg2, arg3, arg4, arg5, arg6 )
{
	relIndex -= 8
	game->SetRain?( RAS[ relIndex ], RAS[ relIndex ], 40, RAS[ relIndex ], 200, 2, 2, 2, RAS[ relIndex ], RAS[ relIndex ], 0.5, RAS[ relIndex ], RAS[ relIndex ] )
	relIndex += 7
}
SceneEffect_FugitiveDust( arg0, arg1, arg2, arg3, arg4, arg5 )
{
	relIndex -= 7
	game->SetDust?( RAS[ relIndex ], RAS[ relIndex ], RAS[ relIndex ], RAS[ relIndex ], 10, RAS[ relIndex ], RAS[ relIndex ] )
	relIndex += 7
}
SceneEffect_Fog( arg0, arg1, arg2, arg3, arg4, arg5 )
{
	relIndex -= 7
	game->SetFog?( RAS[ relIndex ], RAS[ relIndex ], RAS[ relIndex ], RAS[ relIndex ], 10, RAS[ relIndex ], RAS[ relIndex ] )
	relIndex += 1
}
Main(  )
{
	relIndex -= 1
	game->Function_18( false, false, false, false )
	EconomyMode( false )
	InitializeCommon(  )
	game->Function_10(  )
	game->LoadResource( 255, "app:/ui/lyt_HUiMissionCleared.sgo" )
	game->LoadResource( 255, "app:/ui/lyt_HUiMissionFailed.sgo" )
	game->LoadResource( 255, "app:/ui/lyt_HUiFailedResult.sgo" )
	game->Function_14( 255, "fine", "app:/Map/ig_TestLightMap.mac" )
	game->Function_0x10_00(  )
	game->CheckResourcesLoaded(  )
	game->Function_0x10(  )
	game->Function_0x0B(  )
	game->SetMap( "fine", "app:/Map/ig_TestLightMap.mac" )
	game->CreatePlayer( "player" )
	game->PlayBgm( "BGM_E4S02_EDFHonbu" )
	SceneEffect_FugitiveDust( 0.2, 0.1, 0.2, 0.3, 250, 0 )
	game->Function_1E( 10, 0.2 )
}
