To extract RAB, run the tool and type the name of the RAB file, file format must be lowercase (eg: BASIC.RAB will not work, but BASIC.rab will)

To pack RAB, run it through the command promt and use the format: "EDF Tool.exe" /ARCHIVE <Folder Name>
A sample bat file included will showcase how to perform the operation